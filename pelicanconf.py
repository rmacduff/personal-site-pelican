#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Ross Macduff'
SITEURL = 'https://rmac.io'
SITENAME = 'On the shoulders ...'
SITETITLE = 'Ross Macduff'
SITESUBTITLE = 'Ops Guy'
SITELOGO = '/images/rossmacduff.jpeg'

PATH = 'content'

TIMEZONE = 'America/Toronto'

DEFAULT_LANG = 'en'

STATIC_PATHS = ['images']

THEME = "./pelican-themes/Flex"

MAIN_MENU = True

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
#LINKS = (('Pelican', 'http://getpelican.com/'),
#         ('Python.org', 'http://python.org/'),
#         ('Jinja2', 'http://jinja.pocoo.org/'),
#         ('You can modify those links in your config file', '#'),)

#MENUITEMS = (('About', '/about.html'),)
MENUITEMS = (('Archives', '/archives.html'),
             ('Categories', '/categories.html'),
             ('Tags', '/tags.html'),)

# Social widget
SOCIAL = (
            ('linkedin', 'https://ca.linkedin.com/in/rossmacduff'),
            ('github', 'https://github.com/rmacduff'),
            ('twitter', 'https://twitter.com/rmacduff'),
         )

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
