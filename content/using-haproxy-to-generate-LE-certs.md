Title: Letsencrypt, HAProxy, and Nginx
Date: 2016-05-11 06:23
Category: sysadmin
Tags: encryption, http, haproxy, nginx, letsencrypt
Authors: Ross Macduff
Summary: Use HAProxy and Nginx to generate LetsEncrypt certificates

The LetsEncrypt project is a welcome new player to the world Certificate Authorities. The lure of free certificates that can be automatically configured and renewed is something any ops-oriented geek would love to use. Considering the project has only been on the scene for a few months I'd say they're doing quite well in terms of documenting and helping the community, however, it can still be confusing to newcomer to figure out how to actually get a certificate installed.

In this article I am going to give run-down of how I configured HAProxy and Nginx to allow the installation of a new TLS certificate from LetsEncrypt using the [webroot plugin](https://letsencrypt.readthedocs.org/en/latest/using.html#webroot). This method might be of interest to you if:

- You're using HAProxy to perform SSL/TLS termination for your site.
- The DNS (A/CNAME records) currently resolve to this host doing SSL/TLS termination.
- Don't want to use the LetsEncrypt [manual plugin](https://letsencrypt.readthedocs.org/en/latest/using.html#manual).
- Don't want to stop HAProxy to run the LetsEncrypt [standalone plugin](https://letsencrypt.readthedocs.org/en/latest/using.html#standalone).


## Configure an Nginx virtual host

We need to have a locally listening virtual host that will be used for during the ACME [domain validation  process](https://letsencrypt.org/how-it-works/). It will be configured to handle any request that has the path `/.well-known/acme-challenge`. You'll notice that no `server_name` is specified, so this will work regardless of the host specified in the request.

```
server {
    listen 8888 default;

    location '/.well-known/acme-challenge' {
        default_type "text/plain";
        root /location/to/letsencrypt/vhost;
    }
}
```
Reload Nginx.

## Configure HAProxy

Now we need to get HAProxy to send traffic to the LetsEncrypt backend we just configured. First thing we need to do is configure a backend:

```
backend letsencrypt
    server letsencrypt 127.0.0.1:8888
```

And now we need to route any traffic related to the ACME domain validation process to the `letsencrypt` backend. Matching on the `/.well-known/acme-challenge` path of requests is all that's required. In the frontend that's listening on the IP address that your domain is pointing to add this acl:

```
use_backend letsencrypt if { url_beg /.well-known/acme-challenge }
```

Reload HAProxy.

## Generate the certificate

Finally, we can generate the certificate using the `letsencrypt-auto` wrapper script:

```
./letsencrypt-auto certonly --webroot -w /location/to/letsencrypt/vhost --email admin@example.com -d example.com -d www.example.com
```

You can now generate the combined PEM format that HAProxy uses by concatenating the full chain and private PEM files.

```
cd /etc/letsencrypt/live/example.com
cat fullchain.pem privkey.pem > combined.pem 
```
